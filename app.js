'use strict';
var request = require('request');
var moment = require('moment');
var async = require('async');
var users = {};
var configuration = {};
var file;
var input;
var url = {
    'cash_in' :  'http://private-38e18c-uzduotis.apiary-mock.com/config/cash-in',
    'cash_out_natural' :  'http://private-38e18c-uzduotis.apiary-mock.com/config/cash-out/natural',
    'cash_out_juridical' :  'http://private-38e18c-uzduotis.apiary-mock.com/config/cash-out/juridical'
};

// Prototipas naudotojo duomenims, jį galima išplėsti papildomais duomenimis
function User(user_type) {
    this.user_type = user_type;
    this.week_amount = 0;
    this.last_transaction = 0;
}

// Patikriname ar yra failo argumentas
if (process.argv[2]){
    file = process.argv[2];
}
else{
    throw 'Please specify the file';
}
// Sinchroninis transakcijų konfiguracijų užkrovimas prieš pradedant skaičiuoti.

async.forEachOf(url, function(configuration_url, configuration_key, callback) {
    request.get(configuration_url, function(err,response,data){
        if (err) return callback(err);
        try {
            configuration[configuration_key] = JSON.parse(data);
        } catch (err) {
            return callback(err);
        }
        callback();
    })
}, function(err){
    if( err ) {
        console.log(err);
    } else {
        // kai užkraunamos konfiguracijos nuskaitomas json failas
        readFile();
    }
});

// Funkcija konfiguracijoms gauti, norėjau ją išplėsti ir padaryti asinchronišką,
// kraunant konfiguracijos failus pagal poreikį operacijai, tačiau pritrūkau laiko
// Dabar ji tiesiog ištraukia konfiguracija iš var, bei išmeta klaida kai naudojama neleistina operacija

function getConfiguration(configuration_name, input_transaction, callback) {
    if ( !(configuration[configuration_name] == undefined)) {
        return callback(configuration[configuration_name], input_transaction);
    }
    // nebaigta dalis su async configuracijų užkrovimu kiekvienai užklausai
    else{
        if (configuration_name in url) {
            request({
                url: url[configuration_name],
                qs: {},
                method: 'GET',
                headers: {
                    'User-Agent': 'request'
                }
            }, function (error, response, data) {
                if (error) {
                    throw error;
                } else {
                    var dataJson = JSON.parse(data);
                    configuration[configuration_name] = dataJson;
                    return callback(dataJson, input_transaction);
                }
            });
        }
        else{
            throw 'Cannot found configuration URL for '+ configuration_name;
        }
    }
}

// JSON input failo skaitymo funckija
function readFile() {
    var fs = require('fs');
    fs.readFile(file, 'utf8', function(err, data) {
        if (err){
            throw err;
        }
        try {
            input = JSON.parse(data);
        } catch (err) {
            throw err;
        }
        processInput();
    });
}

// funkcija nustato į kurį konfiguracijos failą kreiptis
function processInput() {
    async.forEachOf(input, function (value, transaction, callback) {
            var input_transaction = input[transaction];
            if (input[transaction].user_id in users) {
            }
            else {
                users[input[transaction].user_id] = new User(input[transaction].user_type);
            }
            var transaction_type = input[transaction].type;
            if (transaction_type == 'cash_out') {
                transaction_type = transaction_type + '_' + users[input[transaction].user_id].user_type;
            }
            getConfiguration(transaction_type, input_transaction, function (data, input_transaction) {
                calculateCommissions(input_transaction.user_id, data, input_transaction.date, input_transaction.operation);
            });
    })
}

//Komisinius skaičiuojanti funkcija, ilga ir sudėtinga, reikėtų ją refactorint į daugiau funkcijų pagal skirtingas operacijas.
function calculateCommissions(user_id, transaction_configuration, date, operation){
    var percents = transaction_configuration.percents;
    var amount = operation["amount"];
    var amount_commissions = amount;

    if (!(transaction_configuration["week_limit"] == undefined)) {
        var transaction_date = moment(date, 'YYYY-MM-DD');
        var transaction_date_week = moment(date, 'YYYY-MM-DD').isoWeek();
        if (users[user_id].last_transaction == 0) {
            users[user_id].last_transaction = moment(transaction_date);
            users[user_id].week_amount = operation.amount;
            if (users[user_id].week_amount <= 1000) {
                percents = 0;
            }
        }
        else {
            if (users[user_id].last_transaction.isoWeek() == transaction_date_week || users[user_id].last_transaction.year() != transaction_date.year()) {
                if (users[user_id].week_amount <= 1000) {
                    if ((users[user_id].week_amount + amount) > 1000) {
                        amount_commissions = amount - 1000 + users[user_id].week_amount;
                        users[user_id].week_amount += amount;
                    }
                    else {
                        users[user_id].week_amount += amount;
                    }
                } 
                else {
                    users[user_id].week_amount += operation.amount;
                }
            }
            else {
                users[user_id].week_amount = operation.amount;
                if (users[user_id].week_amount > 1000) {
                    amount_commissions = users[user_id].week_amount - 1000;
                }
            }
            if (users[user_id].week_amount > 1000) {

            }
            else {
                percents = 0;
            }
        }
    }
    users[user_id].last_transaction = moment(transaction_date);
    var commissions = amount_commissions / 100 * percents;

    if (!(transaction_configuration.max == undefined)){
        if (commissions > transaction_configuration.max.amount){
            commissions = transaction_configuration.max.amount;
        }
    }
    if (!(transaction_configuration.min == undefined)){
        if (commissions < transaction_configuration.min.amount){
            commissions = transaction_configuration.min.amount;
        }
    }
    process.stdout.write((Math.round(commissions * 100)/100).toFixed(2)+'\n');
}